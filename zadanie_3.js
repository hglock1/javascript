/*
  Napisz funkcję, która jako parametr przyjmie liczbę naturalną wiekszą od 0.
  Funkcja powinna zwrócić losowo utworzony ciag znaków wybranych ze zbioru
  (const alfabet = "abcdefghijklmnopqrstuvwxyz";). Nowo utworzony ciąg znaków
  powinien mieć długosc określoną przez liczbę podaną jako argument wywołania tej funkcji - np.
  generuj(3); -> 'fws';
  generuj(5); -> 'ldqwa';
*/
/*const alfabet = "abcdefghijklmnopqrstuvwxyz";
function generuj(num, arr){
  const splitWord = arr.split('');
  let output = '';
    for (let i = 0; i < num; i += 1){
      const randomIndex = Math.floor(Math.random() * splitWord.length);
      output += arr[randomIndex];
  }
  return output;
}

console.log(generuj(5, alfabet));*/


function myFunction(num){
  const str = "abcdefghijklmnopqrstuvwxyz";
  let splitLetters = str.split('')
  let output = ''
      for(let i = 0; i < num; i++){
          output += splitLetters[Math.floor(Math.random() * splitLetters.length - 1)];
      }
  return output
}

console.log(myFunction(5));