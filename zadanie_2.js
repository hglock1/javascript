/*
Napisz pętlę for, która modyfikuje tablicę ze zwierzętami, sprawiając, że stają się niesamowite! Jeśli nasza początkowa tablica wygląda tak:

const zwierzęta = [
  "pantera",
  "pirania",
  "łasica"
];

...to po wykonaniu pętli powinna wyglądać tak:
[
  "Niesamowita pantera",
  "Niesamowita pirania",
  "Niesamowita łasica"
]
*/

const animals = [
  'pantera',
  'pirania',
  'łasica'
];

function addAdjective(arr, adjective){
  const output = [];
    for(let i = 0; i < arr.length; i += 1){
       output[i] = adjective + arr[i];
       }
       return output;
} 
console.log(addAdjective(animals, 'Niesamowita '));

