/*
Napisz funkcję, która jako parametr przyjmie dowolne słowo
Funkcja powinna zwrócic ciąg znaków złozonych
z znaków, znajdujących się na parzystym indeksie podanego jako parametr slowa
Zastosuj dowolna petle (for, while).
*/

function evenIndexLetters(word){
let output = '';
  for(let i = 0; i < word.length; i += 2){
    output += word[i];
  }
  return output;
}
console.log(evenIndexLetters('czesc'));